+++
# About/Biography widget.
widget = "about"
active = true
date = 2016-04-20T00:00:00

# Order that this section will appear in.
weight = 5

# List your academic interests.
[interests]
  interests = [
    "Containers and container orchestration",
    "New Relic",
    "Doing more Python",
    "Learning Rust",
    "Learning R"
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "Hon BSc in Computer Science"
  institution = "York University"
  year = 1997
+++

# About Me

I've been building and operating computing systems for 23 years. I still love it.

Most of my experience is in line of business applications and back end systems integration. My primary languages are C#, Java, and Ruby.

My focus has recently shifted from building application systems to automating operations. There is a lot of low hanging fruit in the DevOps arena; plenty of costs savings to be had.

I've just finished a large project to improve operations and delivery capabilities for https://www.redcross.org and https://www.redcrossblood.org. The move to Adobe Experience Manager gives us a vastly improved digital marketing toolset. The move to AWS and Docker gives our back end infrastructure the reliability we need to handle 30x traffic spikes during times of disaster.

I'm from Toronto and currently live in Amherst, NY with [my wife](https://www.deeromito.com) and 2 rapidly growing kids.